//
//  ViewController.swift
//  Custom UI Elements presentation
//
//  Created by Dobromir Dikov on 2/12/15.
//  Copyright © 2015 Dobromir Dikov. All rights reserved.
//

import UIKit

class ViewController: UIViewController, CustomMenuDelegate {

    // MARK: - IBOutlets and IBActions
    
    @IBOutlet weak var customMenu: CustomMenu!
    
    // MARK: - Core functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //set delegate listener of our customMenu class
        customMenu.delegate = self
        
    }

    // MARK: - Delegate Protocol methods from CustomMenu class
    
    func pendingButtonClicked() {
        displayAlert("Pendings button", message: "The button was clicked in CustomView XIB and fired delegate method in ViewController", viewcontroller: self)
    }
    
    func todoButtonClicked() {
        displayAlert("TO DO button", message: "The button was clicked in CustomView XIB and fired delegate method in ViewController", viewcontroller: self)
    }
    
    func showCompleteButtonClicked() {
        displayAlert("Complete tasks button", message: "The button was clicked in CustomView XIB and fired delegate method in ViewController", viewcontroller: self)
    }
    
    func milestonesButtonClicked() {
        displayAlert("Milestones button", message: "The button was clicked in CustomView XIB and fired delegate method in ViewController", viewcontroller: self)
    }
    
    func profilePictureTapped() {
        displayAlert("Profile Pic", message: "The picture was tapped in CustomView XIB and fired delegate method in ViewController", viewcontroller: self)
    }
    
    
    // MARK: - Support methods
    
    func displayAlert(title: String, message: String, viewcontroller: UIViewController) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        viewcontroller.presentViewController(alert, animated: true, completion: nil)
    }


}

