//
//  CustomMenu.swift
//  Turtle Planner 02a
//
//  Created by Dobromir Dikov on 29/11/15.
//  Copyright © 2015 Dobromir Dikov. All rights reserved.
//

import UIKit

@objc protocol CustomMenuDelegate{
    optional func pendingButtonClicked()
    optional func showCompleteButtonClicked()
    optional func milestonesButtonClicked()
    optional func profilePictureTapped()
    optional func todoButtonClicked()
}

@IBDesignable
class CustomMenu: UIView {
    
    
    var delegate:CustomMenuDelegate?
    
    
    
    // Our custom view from the XIB file
    var view: UIView!
    
    // MARK: - Outlets and Actions to delegate
    
    
    @IBOutlet weak var imageView: UIImageView!

    
    @IBOutlet weak var todoBtnOutlet: UIButton!
    @IBAction func todoBtnAction(sender: AnyObject) {

        delegate?.todoButtonClicked!()
    
    }
    
    
    @IBOutlet weak var showCompleteBtnOutlet: UIButton!
    @IBAction func showCompleteBtnAction(sender: AnyObject) {
        
        delegate?.showCompleteButtonClicked!()
        
    }
    
    
    @IBOutlet weak var milestonesBtnOutlet: UIButton!
    @IBAction func milestonesBtnAction(sender: AnyObject) {
        
        delegate?.milestonesButtonClicked!()
        
    }
    
    
    @IBOutlet weak var pendingBtnOutlet: UIButton!
    @IBAction func pendingBtnAction(sender: AnyObject) {
        
        delegate?.pendingButtonClicked!()
        
    }
    
    
    
    // MARK: - XIB view setup and load
    
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "CustomMenu", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        
        return view
    }
    
    
    
    // MARK: - UIView
    
    override init(frame: CGRect) {

        super.init(frame: frame)
        
        xibSetup()
        initUI()
    }
    
    required init?(coder aDecoder: NSCoder) {

        super.init(coder: aDecoder)
        
        xibSetup()
        initUI()
    }
    
    
    
    // MARK: - Private
    
    private func initUI() {
        
        self.imageView.layer.cornerRadius = imageView.frame.size.width / 2
        self.imageView.layer.borderWidth = 1.0
        self.imageView.layer.borderColor = UIColor(red: 111/255, green: 203/255, blue: 113/255, alpha: 1.0).CGColor
        self.imageView.clipsToBounds = true
        
//        imageView.image = UIImage(named:"dikov.jpg")
        self.imageView.layer.zPosition = 999.0
        
        
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:Selector("profilePicTapped"))
        imageView.userInteractionEnabled = true
        imageView.addGestureRecognizer(tapGestureRecognizer)
        
    }
    
    func profilePicTapped () {
        delegate?.profilePictureTapped!()
    }
    
}






